export default {
  theme: {
    themes: {
      light: {
        primary: "#DE1D9D",
        primary2: "#F8D2EB",
        primary3: "#FBE8F5",
        secondary: "#212CFF",
        secondary2: "#D3D5FF",
        secondary3: "#E8E9FF",
        accent: "#8c9eff",
        error: "#b71c1c",
        gray: "#f7f8f9",
        gray2: "#DADADA",
        gray3: "#939393",
        signoutBg: "#FFE9ED",
        SuccessMsg: "#F1FFF2",
        ErrorMsg: "#FAE1E4",
        SuccesInput: "#7FC582",
      },
    },
  },
};
