export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head() {
    // Get the current locale
    const currentLocale = this.$i18n.locale;
    const currentDir = this.$i18n.locales.find(
      (locale) => locale.code === currentLocale
    ).dir;

    return {
      htmlAttrs: {
        lang: currentLocale,
        dir: currentDir,
      },
      title: "nuxtwebsiteproject",
      meta: [
        { charset: "utf-8" },
        { name: "viewport", content: "width=device-width, initial-scale=1" },
        { hid: "description", name: "description", content: "" },
        { name: "format-detection", content: "telephone=no" },
      ],
      link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
    };
  },
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["@/assets/scss/main.scss", "@mdi/font/css/materialdesignicons.css"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ["~/plugins/vuelidate.js"],
  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ["@nuxtjs/axios", "@nuxtjs/i18n", "@nuxtjs/vuetify"],
  i18n: {
    locales: [
      {
        code: "en",
        name: "English",
        iso: "en",
        file: "en.json",
        dir: "ltr",
      },
      {
        code: "ar",
        name: "Arabic",
        iso: "ar",
        file: "ar.json",
        dir: "rtl",
      },
    ],
    defaultLocale: "en",
    lazy: true,
    langDir: "lang/",
    vueI18n: {
      fallbackLocale: "en",
    },
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: "/",
  },
  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    optionsPath: "./plugins/vuetify.js",
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
};
